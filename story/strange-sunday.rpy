# define circe = Speaker('Circe', voice_woman, color='#8b0000')

label scenario_strange_sunday:
    outfit yui casual

    scene bg yui livingroom dark
    show yui a_3 at centerright, faceleft
    show yuuna a_0 at centerleft, faceright

    $ sandra.name = "Sandra Davis"
    $ yuuna.name = "Mom"

    yui "...sick of babysitting Natsumi all the time, mom!"

    show yuuna a_3
    yuuna "Look, it's just one night. We made these plans months ago!"

    yui "I had plans too! You knew about this! Can't--"

    yuuna "Look, I didn't realize they were on the same day. I'll make it up to you, ok?"

    yuuna "If the party's so important, maybe you can just bring Natsumi?"

    show yui a_2
    yui "I can't do that, I'd look like a huge dork if-"

    "*knock knock*"
    outfit sandra casual
    show sandra b_1 at left with moveinleft

    sandra "Yuuna~~~ you left the door unlocked, that means I get to go through your-"

    show sandra b_8
    "Reading the room, Ms. Davis froze."

    sandra "Am I interrupting something?"

    yuuna "No, no, everything's fine! We were just having a bit of a tiff."

    show sandra b_2
    sandra "Ah, ok. I'll make my way back outside, then."

    show sandra b_2 at offscreenleft with moveoutleft

    sandra "In the car!"

    pause .5

    show yuuna a_2
    yuuna "I'm sorry, can we pick back up in the morning? I'm holding everyone up."

    show yui a_6
    yui "...yeah"

    show yui a_5
    yui "I guess."

    show yuuna a_0
    yuuna "Thank you, Yui."

    show yuuna a_3
    yuuna "I'll make this up to you, I swear."

    show yuuna a_3 at offscreenleft with moveoutleft

    yuuna "Bye Yui! Bye Natsumi! Love you!"

    "*slam*"

    show yui a_2
    yui "RAAAUUUUHGGGGGG!!!!"

    "I threw a pillow against the wall, which limply slid back down onto the floor."

    show yui a_3
    yui "You idiot! I can't believe you left me stuck babysitting {b}again{/b}!"

    show yui a_5
    yui "I feel like I'm more of an adult than you are!"

    outfit natsumi casual

    show natsumi b_0 at centerleft, faceright

    natsumi "Were you and mommy fighting, again?"

    show yui a_3
    yui "Yeah, a little bit."

    show natsumi b_1
    natsumi "It's because of me, isn't it?"

    show yui a_6
    yui "No no no no, nothing like that!"

    show yui a_5
    yui "It was just about... how Mom will schedule things without... telling me."

    yui "Or forget, in this case."

    show natsumi b_4
    natsumi "Well, if you want to go to the party, can't you just take me with?"

    show yui a_3
    yui "Certainly not."

    yui "John and his friends may not judge as harshly as others, but I'm still not bringing my little sister along."

    show natsumi b_7
    natsumi "Oh... I'm sorry..."

    pause .5

    show yui a_5
    yui "It's ok, it's not your fault."

    pause .5

    show natsumi b_0
    natsumi "Well, if you do have to stay here, can we play Furry Forest? It's bug catching day!"

    show yui a_1
    yui "Yeah... we can do that."

    scene bg yui room night computer on
    show yui a_0 at center
    with dissolve

    show yui a_5
    think "Well, Natsumi's finally in bed..."

    "Pulling out my diary, I filled out an entry for today."

    "After I finished jotting down my sorrows, I cracked open Harold Planter and the Cellar of Skeletons."

    show yui a_7
    "As I read, my eyes drooped more, and more."

    think "Oh Harold... you wouldn't ditch me to hang with your friends and make me watch my sister... would you..."

    think "You'd cancel your plans so I could go out instead..."

    show yui a_7
    think "..."

    yui "I wish my mom would grow up, already..."

    scene black
    with dissolve

    "Zzz..."

    yui "Zzz... snrt..."

    "Drifting back into consciousness, I had realized I had fallen asleep at my desk."

    think "I really ought to move over to the bed, but I'm so comfy..."

    think "Ah well, I guess I'll get up..."

    scene bg yui room night computer on
    show yui a_0 at center
    show circe a_0 at centerright, faceleft
    with dissolve

    circe "Salutations, human!"

    show yui b_2 at centerleft with ease
    yui "{size=+5}{b}W-whaaa!!!{/b}{/size}"

    circe "Woah, keep it down!"

    yui "W-w-w-w-what are you!?!"

    circe "Me? I'm a fairy! I grant wishes to give people happiness!"

    show yui a_6 at center with ease
    yui "This can't be real. Am I still dreaming?"

    show circe a_5
    circe "Very astute, girl! I suppose you could say you are!"

    show circe a_2
    circe "I couldn't help but overhear your wish, and thought this was a perfect opportunity to grant a little more happiness for the world!"

    "She picked up my diary and licked a finger as she leisurely leafed through it."

    show circe a_4
    circe "So much gloominess. So much loneliness."

    show circe a_3
    circe "It tears me apart! Such a poor, sweet girl subjected to something as cruel as this... baby sitting!"

    "Finishing, the diary disappeared out of her hands, and reappeared onto my desk."

    "The fairy poofed an ancient book full of yellowed parchment parchment out of thin air, quill floating beside it."

    show circe a_3
    circe "But don't worry, I have the solution right here."

    show circe a_0
    circe "Just give the first page a read over, and if it all looks good, sign away!"

    show yui a_5
    "I took the tome in my hands, noticing an odd delay between my hands moving and the paper moving."

    "On the front, it said \"Wish Grimore\"."

    "Bringing it closer to my face, and turning to the first page, I notice the book was semi-translucent, with cursive handwriting scrawled across it."

    nvl clear
    manu "Wish Contract For Yui Yamashita"
    manu "By signing this paper, the following wish will be granted: \"Make Yuuna Yamashita Grow Up, And Learn To Assume The Responsibilities Of A Mother\"."
    manu "This wish will be fulfilled at the own discretion of the contract fulfiller, Circe."
    manu "Upon granting this wish, the fulfiller will be allowed to request another wish. A wish may not be requested until the first is fully granted."
    manu "If the recipient of the wish's effects desires additional wishes, they may share the book with another party, assuming that person is not the subject of the wish."
    manu "To bind this wish into reality, sign here: ___________"

    circe "Simple enough, right?"

    yui "Sign here... and my mom will stop dumping Natsumi on me?"

    circe "Yes!"

    show circe a_5
    extend " I guarantee it!"

    show circe a_3
    extend " 100%!"

    show yui b_1
    yui "Hmm..."

    think "This is very obviously too good to be true..."

    extend " and she keeps making all those creepy faces."

    think "But it is just a dream, right?"

    show yui b_2
    think "Wasn't there a type of demon in a Harold Planter book that did something like this?"

    think "They'd offer you a wish that vaguely sounded like what you wanted, only to completely reinterpret it as something they could use to torment you..."
    menu:
        "Even if this is a dream, is this really such a good idea?"
        "{s}I signed it{/s}":
            placeholder

        "I refused":
            show yui b_1
            yui "On second thought... no. I think I'm ok."

            show yui b_0
            yui "Being in highschool is crazy enough, I don't need {i}questionable magic contracts{/i} to make it even crazier."

            show circe a_5
            circe "Wow, really? I'm surprised! {size=-5}Now I'll have to find some other sucker...{/size}"

            show circe a_0
            circe "Ah well, your loss!"

            "A blast of white powder exploded out of the fairly."

            show yui b_3
            "I tried to shut my eyes, but the powder had already hit my face in full force."

            yui "Agh, what is this stuff? It tastes like dish so{size=-5}ooo...{/size}"

            scene black

            circe "{i}Sweet dreams!{/i}"

            "*thump*"

            scene bg yui room day
            with dissolve

            "*alarm noises*"

            show yui a_7 at center

            "A beeping alarm and sunlight peeking through the curtains clued me in that it was now morning."

            show yui b_2

            think "What a weird dream... it felt so real..."

            show yui a_0

            "I pulled out my diary, jotting down as much as I could remember."

            extend " Which a surprisingly large amount, actually."

            "Normally I lost one or two details, but this was solid the entire way though."

            show yui a_6

            think "Almost as if it were real..."

            "*thump thump thump*"

            yuuna "Yui, are you doing ok? It's already 7:00! You're usually up by now!"

            think "What's she talking about, it's only..."

            show yui a_2

            yui "Oh no! I slept in! I'm running late for being early!"

            "Rushing through my daily routine, I quickly got dressed, showered, and brushed my teeth."

            outfit yui uniform

            scene bg yui livingroom day
            show yuuna a_0 at centerleft
            with dissolve

            show yui a_1 at centerright, faceleft with moveinright

            "Belting into the kitchen, a sunny-side up egg over rice awaited me."

            show yui a_6

            extend " And an apologetic looking mother."

            show yuuna a_2

            yuuna "I'm sorry last night went so poorly for you, Yui."

            show yui a_5

            yui "It's ok, I just get like that sometimes..."

            show yuuna a_4

            yuuna "No, it's not. I made plans without checking with you, and then was selfish enough to think mine were more important than yours!"

            yuuna "The girls were giving me a very hard time last night, and I deserved it."

            show yuuna a_3

            yuuna "So, as a way to make up for it, I thought we could plan a trip to Magic World at the end of the month!"

            show yui a_2

            yui "R-really? You mean it?"

            show yuuna a_0

            yuuna "Of course! I want to make things right."

            show yuuna a_1

            yuuna "You can even bring a friend!"

            show yui a_0

            "I'd been asking about going for what seemed like forever."

            "But whenever I brought it up, there was always some reason why we couldn't go."

            "Sometimes we didn't have the money, other times Mom just had too much going on at work to take a break."

            "I started tearing up a bit."

            show yuuna a_2

            yuuna "I talked it over with Ms. Davis last night, she said she's perfectly fine if you want to invite John-{nw}"

            yuuna "I talked it over with Ms. Davis last night, she said she's perfectly fine if you want to invite{fast} Sandy to come with."

            show yui a_5

            think "Huh... something felt a weird about that last part... like mom was initially talking about someone else..."

            pause 0.5

            show yui a_1

            think "Why would she do that, though? Sandy is my best friend, after all!"

            show yui a_0

            yui "Thanks, mom! She'll be so excited!"

            show yuuna a_1
            show yui a_1:
                xpos 0.35
            with ease
            # go in for hug

            pause .25

            show yui a_7:
                xpos 0.3
            with ease

            yui "This means a lot to me. Thank you."

            pause 1.0

            show yuuna a_0
            yuuna "Oh, look at the time! You'll need to leave soon if you want to be early!"

            show yui b_2 at center with ease

            yui "Shoot, you're right!"

            "Scarfing down the rest of my food, I grabbed my bag and ran for the door."

            show yui a_0 at offscreenleft with moveoutleft

            yui "Bye, I love you!"

            show yuuna a_1 at faceleft
            yuuna "I love you too, have a good day!"

            body sandra strangesunday_cynthia
            outfit sandra uniform

            body john strangesunday_johnGBAP
            outfit john formal

            $ sandra.name = "Sandra"
            $ john.name = "Jane Davis"

            scene bg school classroom hallway day
            show sandra d_3 at centerright, faceleft
            with dissolve

            show yui a_1 at centerleft, faceright with moveinleft

            yui "~Magic World, Magic World~"

            show yui b_5

            think "Oh, there's Sandy now!"

            "I'd known Sandra Davis since my early childhood. She and Mom would take turns looking after me and John-{nw}"

            "I'd known Sandra Davis since my early childhood.{fast} Ms. Davis and Mom would take turns looking after us when we were kids."

            show yui a_0

            yui "Hi Sandy, you're here early!"

            show sandra d_4

            sandra "Huh? Am I?"

            yui "Well... yeah! Were you getting tired of my lectures for coming in late?"

            show sandra c_3

            sandra "Uh... yeah, right."

            show sandra c_2

            sandra "My, uh..."

            "Sandy's eyes darted around, as if searching for the answers to what should be our normal banter."

            sandra "My... mom... gave me a ride over..."

            yui "Aw, that's so nice of her! You're so lucky to have a mom that's a teacher!"

            show sandra c_3

            sandra "Yeah... lucky..."

            show sandra d_3 at left with ease

            "With that, Sandy started walking off, rather aimlessly."

            show yui b_5 at center, faceleft with ease
            # show yui at faceleft

            yui "Where are you going? Class is about to start!"

            show sandra d_3 at faceright

            sandra "Yeah, I know that."

            sandra "I just kind of..."

            show sandra c_5 at faceright

            extend " forgot where it was."

            yui "Is this some kind of prank? It's right here! We're in the same first period, silly!"

            scene bg classroom 1
            with dissolve

            show yui a_0 at center, faceleft with moveinright
            show sandra d_5 at centerright, faceleft with moveinright

            "Motioning to her, I walked inside the classroom, and took a seat, saving the one next to me."

            show yui a_0 at faceleft:
                linear .5 ypos 1.1
            show sandra d_5 at centerright, faceleft:
                linear .5 ypos 1.1

            "That rather odd first encounter aside, class went as well as ever."

            show yui a_0 at faceleft:
                ypos 1.1
            show sandra d_5 at faceleft:
                ypos 1.1

            nvl clear

            nvl_narrator "Sandy kept dozing off in class, but every time Connie called on her with a question, she'd pick her head up and answer like it was no big deal."

            nvl_narrator "Eventually, Connie just let her nap."

            nvl_narrator "When we got to group worksheets, Sandra helped me out a lot. Honestly, it felt like she should be the teacher!"

            show sandra b_0 at centerright, faceright:
                ypos 1.1

            yui "Wow, I can't believe how good you are at this! Normally, I have to drag {i}you{/i} through math!"

            yui "Now, how about the area under this intersection of polygons? I think I'm most of the way there, but I keep getting my functions mixed up..."

            show sandra b_1

            sandra "Ah, yeah, this one's simple! You just...!"

            show sandra d_3

            sandra "You uh, just..."

            sandra "..."

            show sandra d_5

            sandra "I have no idea what any of those words mean."

            show yui a_3
            yui "What!? You answered so confidently in class!"

            show sandra c_4

            sandra "Lucky guess?"

            show yui b_2
            yui "Fourteen detailed lucky guesses in a row!?"

            show sandra d_3

            sandra "I'm a lucky gal!"

            scene black with dissolve

            "With first period over, I figured the rest of the day would be a breeze."

            "What I didn't expect was the same song and dance with the other Davis."

            scene bg classroom 3 day
            show john a_4 at centerleft, faceright
            show yui a_0 at centerright, faceleft:
                ypos 1.1
            with dissolve

            john "Erm... good morning, class..."

            "Ms. Davis looked visibly shaken. Her nervousness was a far cry from her normal peppy attitude."

            think "Did something bad happen to those two?"

            think "Well if they're not feeling too well, maybe I can pick up the extra slack!"

            show yui b_0
            yui "Ohayou, Davis-sensei!"

            show john a_5
            john "{size=-5}Oh, right...{/size} Ohayou, Y... Yui-san!"

            "Ms. Davis took out a small folder, frantically leafing through it."

            show john a_6
            john "Today, we'll be talking about... erm..."

            show john a_5
            john "{size=-5}No, that's not it...{/size}"

            show yui a_6
            "I could hear a bit of muttering between my classmates."

            "Is Ms. Davis ok? She's acting really weird..."

            "Should I get the nurse?"

            show yui a_3
            "I raised my hand."

            show john a_4
            john "Yes?"

            show yui a_1
            yui "I believe we were going to talk about ru-verb conjugations today, right?"

            show john a_3
            show yui a_0
            john "Ru-verbs?"

            pause 1.0

            show john a_2
            john "Oh, y-yes! That's what I had planned! Arigatougozimasu, Yui-san!"

            john "Sorry, class, I've been having a bit of a rough morning."

            show john a_1
            john "Now, if you'll turn to page 89 of your textbook..."

            nvl clear

            nvl_narrator "After I'd gotten Ms. Davis on track, class went mostly back to normal."

            nvl_narrator "If someone asked a question that was further in the material, Ms. Davis would still get a bit flustered, though."

            nvl_narrator "Since I'd been reading ahead, I was able to help with those."

            nvl_narrator "The strange thing was, after I'd given my answer, Ms. Davis would praise me, and then give a more exhaustive answer!"

            scene bg lunch
            show sandra d_3 at centerright, faceleft:
                ypos 1.1
            with dissolve

            show yui a_0 at offscreenleft

            show yui a_0 at center
            with moveinleft

            "In the cafeteria, I found Sandy sitting by herself, nibbling on some fries."

            show yui a_1
            yui "Hey, Sandy!"

            show sandra d_0
            sandra "Hi Yui, take a seat."

            show yui a_6:
                ypos 1.1
            with ease
            yui "Your mom was acting weird, today. She forgot somehow forgot her whole lesson plan!"

            show yui a_1
            yui "Good thing I was there to remind her!"

            show sandra f_0
            sandra "Yeah, I would expect as much."

            show yui a_6
            yui "What about you, feeling any better?"

            show sandra c_0
            sandra "Yeah a little bit!"

            show sandra c_1
            sandra "Pelting kids in the face dodgeballs helped me loosen up."

            show sandra a_6
            sandra "Plus being surrounded by all those sweaty guys..."

            pause 1.0

            show sandra a_3
            sandra "Ugh, what am I saying? I'm way to old for them!"

            show yui b_2
            yui "Too old? Aren't you 18?"

            sandra "..."

            show sandra a_2
            sandra "Can I trust you to keep a secret?"

            show yui b_5
            yui "What? Of course! You're my best friend!"

            sandra "Ok, listen closely..."

            "Leaning over, Sandy whispered into my ear. I could feel her breath against my neck."

            show sandra a_1
            sandra "{i}{size=-5}I'm actually 18.{/size}{/i}"

            pause .5
            show yui b_2
            yui "{i}{size=-5}...yes?{/size}{/i}"

            show sandra a_3
            sandra "{i}{size=-5}J-jane is my mom?{/size}{/i}"

            show yui b_1
            yui "{i}{size=-5}...uh huh?{/size}{/i}"

            "Sandy pulled back, looking a bit frustrated."

            show sandra a_4
            sandra "I'm a teenager! No, what I mean is, I'm not an adult! No, I mean..."

            "The distress I'd seen from earlier splashed back across her face."

            show sandra b_3
            sandra "Jane Davis is my mom! I'm her daughter! Ugh!"

            show yui a_5
            think "Why would she keep repeating things that are so obviously true? She sounds so frustrated every time she says it, too..."

            think "I'd just chalk this up as one of her pranks, but I've never seen her mom in on one before."

            show yui a_3
            yui "Ok, ok, this is clearly making you even more upset. Let's change the subject..."

            nvl clear
            nvl_narrator "Once we got off the topic of whatever was bothering her, things went back to normal banter."

            nvl_narrator "Sandy drilled me on boys I liked, but I evaded her questions with perfect precision."

            show sandra c_0
            sandra "What about Claus? He seems like the intellectual type."

            show yui a_2
            yui "N-nothing special!"

            show sandra c_0
            sandra "I took a picture of him changing in the locker room, you wanna see it?"

            show yui a_3
            yui "What!?!"

            pause 1.0

            show yui a_6
            yui "This is a cat."

            show sandra g_1
            sandra "Perhaps, but you seemed real eager to look."

            show yui a_2
            yui "This confirms nothing!!!!"

            nvl_narrator "I asked Sandy if she could to hang out after school, so I could further question her."

            nvl_narrator "Maybe it had something to do with stress from school?"

            scene bg school entrance dusk
            show yui a_0 at centerleft
            with dissolve

            "As I was getting ready to head home, I got a text from Sandy."

            text start yui
            text title "Sandy <3"
            msg sandra "Mom wants help cooking dinner, is it ok if you come over here?"
            msg yui "Sure thing!"
            text end

            scene black with dissolve

            outfit john casual

            scene bg main livingroom dusk
            show john a_0 at center
            with dissolve

            "*knock knock*"

            show yui a_0 at centerright, faceleft with moveinright

            show john a_2
            john "Good evening, Yui!"

            show john a_1
            john "Sandra mentioned you were coming over for dinner. She's already in the kitchen."

            show john a_0 at offscreenleft, faceleft with moveoutleft

            scene bg main kitchen dusk
            show john a_0 at center, faceleft
            show sandra a_0 at centerleft, faceright
            show yui a_0 at centerright, faceleft:
                ypos 1.1
            with dissolve

            show sandra a_1
            sandra "You can have a seat over there, this shouldn't take long, right Mom?"

            show john a_4
            john "R-right!"

            show yui a_1
            yui "What are you making?"

            show sandra b_0
            show yui a_0
            sandra "Oh, just some curry!"

            show sandra b_2
            sandra "You you can cut the potatoes, I'll start on the onions."

            show john a_7
            john "Of course!"

            "As the two girls got to work, I was impressed by how much charge Sandy was taking."

            "She kept leaning over to give Ms. Davis pointers on how she was cutting, as she bounced between measuring spices and caramelizing onions."

            show yui b_2
            think "When did Sandy learn so much about cooking?"

            show yui a_6
            extend " Especially about curry. That's Ms. Davis's specialty!"

            show john a_9
            john "Oh, hold on Sandy, we don't want to use that brand of curry powder."

            show sandra e_4
            sandra "Huh?"

            show john a_10
            john "I have a nicer blend in the cabinet over here, let me fish it out..."

            show john a_1
            john "Make sure you keep stirring the onions, too. They'll burn if you're not careful!"

            nvl clear

            nvl_narrator "Despite the night starting with Sandy leading, Ms. Davis gradually started overtaking her."

            nvl_narrator "By the end, Ms. Davis was lecturing her on the ins and outs for getting different taste profiles."

            show john a_2
            john "...I like to add some yogurt in near the end, it makes the curry a little creamier and balances out the spices."

            show sandra f_2
            sandra "Is this much enough?"

            show john a_9
            john "I'd go a little lighter. Remember, you can always add more spice later, but you can't take it back out."

            show sandra f_0
            sandra "Ah, ok! I'll only add half, then."

            show john a_2
            john "Perfect!"

            scene bg main kitchen dusk
            show john a_0 at centerright, faceleft:
                ypos 1.1
            show sandra a_0 at centerleft, faceright:
                ypos 1.1
            show yui a_0 at center, faceleft:
                ypos 1.1
            with dissolve

            "As we sat down to eat, the other member of the Davis family came down from her room."

            show holly b_8 at left, faceright with moveinleft

            holly "Hey guys, dinner ready? I'm starving."

            show john a_4
            "Ms. Davis froze, a look of awe on her face at the sight of Holly."

            show holly b_2
            holly "What? Is something wrong, Mom?"

            show john a_5
            john "Uh, n-no, everything's fine, it's just been so long since we talked!"

            holly "Huh? We talked yesterday!"

            show sandra c_3
            sandra "Uh, she means it feels like it's been so long!"

            show sandra b_2
            sandra "You know how she gets when get finishes one of her Otome games!"

            show holly b_1
            holly "Aww, I realize she'd finished another so soon."

            pause 1.0

            show john a_1
            john "W-well, anyways... Sandy and I made curry! Help yourself!"

            show holly b_0
            holly "Great!"

            show holly a_1:
                ypos 1.1
            with ease

            "After fixing a plate for herself, Holly joined us at the table."

            holly "How's it going, Yui? Keeping my sister out of trouble?"

            show yui b_5
            yui "You bet!"

            yui "I've gotten real good at sniffing out her schemes."

            show yui b_0
            yui "Holding her back from pulling the fire alarm has helped me build my upper body strength."

            show yui a_0
            yui "It's a hard thankless job, but I take pride in my work."

            show yui b_1
            yui "She was really quiet today, though. I was half expecting it all to be a ruse to let by guard down..."

            holly "Haha, Yui's finally rubbing off on you! You'll be an honor student and a member of the student council next week for sure."

            sandra "What? No way!"

            "Holly ruffled Sandy's hair."

            holly "You'll need some nerd glasses, while you're at it."

            show sandra g_3
            sandra "Waah, cut it out! I just brushed it!"

            holly "Well I guess you'll just have to brush it again!"

            "Despite looking upset, I could tell Sandy was really enjoying herself."

            think "I wonder if Natsumi would enjoy stuff like that..."

            pause 1.0

            show yui b_5
            yui "Oh! I just remembered!"

            yui "I'm going on a trip with my Mom at the end of the month, and she said I could bring Sandy!"

            show sandra b_0
            sandra "Oh yeah! To Magic World, right?"

            show john a_4
            john "Magic World?"

            show yui b_2
            yui "Huh? My mom called you about it earlier today, didn't she?"

            yui "And how did you already find out, Sandy? I was going to surprise you!"

            pause 1.0

            show john a_2
            john "Oh, right, yes Yuuna called me about it. Sounds like fun!"

            show sandra d_4
            sandra "I mean, where else would we be going?"

            show sandra c_1
            sandra "You just played right into my trap, that's all! It was a guess!"

            think "Hmm... something is definitely going on with these two..."

            show yui b_0
            yui "Buuut I guess if you already know, we might as well plan out what we're going to do there! I've working out our schedule!"

            nvl clear

            nvl_narrator "The rest of dinner went as well as ever, with lively chatter coming from all parties."

            nvl_narrator "Ms. Davis and Sandy continued to do that odd thing, though, where they temporarily forgot things they should know."

            nvl_narrator "Someone would bring something up, they'd get confused, and then seem to remember it a couple moments later."

            nvl_narrator "After we finished eating and helped Ms. Davis clean the dishes, Sandy and I headed up to her room."

            scene main room night
            show yui a_0 at centerright, faceleft
            show sandra d_0 at centerleft, faceright
            with dissolve

            yui "Your mom always makes the best food, I sure am glad we decided to come over here!"

            show yui a_1
            yui "Though it didn't seem like she needed quite as much help as I thought she would, given your text..."

            show sandra c_1
            sandra "Well, she was trying something a bit new to her, I guess she wasn't sure how much work it'd be!"

            show yui a_6
            yui "Something new? Your Mom makes curry all the time! It's her staple!"

            pause 1.0

            show sandra c_3
            sandra "Ah, yeah, you're right. I don't know what I was thinking..."

            show yui a_3
            yui "And there it is again!"

            show sandra c_2
            sandra "Where was what now?"

            show yui a_2
            yui "That thing! That thing you've been doing all day!"

            yui "You keep saying weird stuff or forget things, but as soon as I correct you or remind you you're suddenly ok and back to normal!"

            show yui a_6
            yui "And then there was that thing at lunch, where you were acting like being a teenager was suddenly out of the ordinary..."

            show sandra c_3
            sandra "!!!"

            show yui a_5
            yui "If I didn't know any better, I'd think you had some kind of curse cast on you..."

            show sandra c_4
            sandra "!!!!!!"

            show yui a_6
            yui "I can rule that one out, though. Curses aren't real, right?"

            show sandra c_5
            sandra "Nnnnrnnnn-rriight!"

            yui "..."

            yui "Are curses real?"

            show sandra c_3
            sandra "Curses are definitely {i}not{/i} real."

            sandra "Curses are not not not real."

            sandra "Curses are definitely not not not not not not real."

            show sandra c_4
            sandra "I have not not not not not not not not not been cursed!"

            show sandra c_5
            sandra "I have not not not not not not not not not not not not not not not not not not not not not not not not not not not-"

            yui "Ok, I get it! You haven't been cursed!"

            extend " Or maybe you have? It's hard to keep track of that many nots..."

            show sandra c_3
            sandra "At least one of those statements is definitely true."

            show yui a_3
            yui "Well yeah, it's a binary statement."

            show yui a_3
            yui "Well, even if this is some elaborate prank, I guess I'll play along."

            yui "Ok Ms. Cursed Teen, are you able to tell any details about this curse?"

            show sandra c_2
            sandra "No."

            show yui b_1
            yui "Did you steal a pirate's gold?"

            sandra "No."

            yui "Did you wish on a monkey's paw?"

            sandra "No."

            yui "Did you make a deal with the devil?"

            show sandra g_5
            sandra "Yrrngggnnno."

            yui "..."

            show sandra g_3
            sandra "My Mom also did not make a deals with any kind of demon."

            show yui b_2
            yui "{b}Your mom made a deal with a demon?!?{/b}"

            show yui a_5
            yui "That'd explain why she's acting weird, too!"

            yui "Is that why all the boys in our class have a crush on her?"

            show sandra e_5
            sandra "All the boys in our class have a crush on my mom!?"

            yui "Apparently not."

            yui "Is that... why she looks so young for her age?"

            show sandra d_5
            sandra "Sort of?"

            nvl clear

            nvl_narrator "As my interrogation continued, I still wasn't getting any closer to having to foggiest idea of what was going on."

            nvl_narrator "From what I could gather, Ms. Davis made some sort of deal with a demon."

            nvl_narrator "The curse to made Jane prettier and smarter, and made Sandy younger and more of an airhead, but didn't affect Holly."

            nvl_narrator "But this didn't add up at all! That's how they've always been! How could a curse change them and simultaneously do nothing?"

            nvl_narrator "Eventually, we decided to give the interrogation a rest, switching to a movie."

            show sandra e_2 at center, faceleft
            sandra "Shirt! Off! Shirt! Off! Shirt! Off!"

            show yui a_3
            yui "It's getting there, hold on!"

            show sandra c_3
            sandra "Nooo! I want to see Erwin's hot hot body noooow!!!"

            "The Dusk series didn't have anywhere near the imaginative world or magic adventures that Harold Planter did, but it did have one thing."

            "Hot boys that take their shirts off."

            show sandra g_6
            sandra "Eeeeeeeeeee!!!"

            "Sandy violently shook back and forth as she deathgripped a pillow her chest."

            "*knock knock*"

            show john a_7 at centerleft with moveinleft
            john "Is everything alright in here? I thought I heard a death wail!"

            show yui b_0
            yui "It's fine, we're just watching dusk."

            show john a_4 at faceleft
            john "Oh... yeah, you are..."

            "Ms. Davis's eyes glued themselves to the screen."

            "Her breathing seemed to get a bit shallower, as well."

            show yui b_2
            yui "Are you ok? I thought we'd seen this movie hundred of times by now!"

            show john a_7
            john "Well, yeah!"

            show john a_8
            extend " {size=-5}{i}That doesn't make him any less hot, though...{/i}{/size}"

            pause 2.0

            show john a_4
            john "Um... anyways..."

            pause 1.0

            show john a_3
            john "Ummmmm..."

            pause 1.0

            "Erwin put his shirt back on. Breaking the spell over Ms. Davis."

            show john a_9
            john "Uh, I told your Mom you'd be back by eight, Yui. I can drive you home."

            show yui b_0
            yui "Ok!"

            scene bg yui room night computer on
            show yui a_0 at center
            with dissolve

            think "Boy, what a day!"

            show yui a_1
            think "Sandy's always fun to hang out with, near the end I nearly forgot about all the weird stuff that happened earlier!"

            think "She seemed to claim that she was changed by that curse, but I didn't notice anything..."

            think "If there really was a change maybe I can find some proof in my diary..."

            "Cracking it open, I started skimming through my entries from the past year."

            show yui b_2
            yui "*gasp*!"

            scene bg school classroom hallway day
            show yui a_5 at center, faceleft
            with fade

            show sandra f_0 at left, faceright with moveinleft

            show sandra f_2
            sandra "Hey, Yui! I know I'm almost late!"

            show sandra f_1
            sandra "Buuut I'm not, and that's what counts, right?"

            yui "..."

            show sandra f_3
            sandra "...right?"

            show yui a_5
            yui "Oh, yeah."

            show yui a_6
            yui "I've got something on my mind, sorry."

            show yui b_0
            yui "L-lets get to class!"

            show black with dissolve

            nvl clear

            nvl_narrator "With all the questions racing through my mind, I could barely pay attention in class."

            nvl_narrator "Or to whatever Sandy was gabbing about during lunch."

            nvl_narrator "After school, I asked Sandy if I could see her, alone."

            scene main room night
            show yui a_5 at centerright, faceleft
            show sandra d_3 at centerleft, faceright
            with dissolve

            sandra "You ready to talk about what's getting {i}you{/i} down?"

            show yui a_5
            yui "Yeah."

            "I pulled out my diary, leafing through it as I talked."

            show yui a_6
            yui "I've kept this diary for years. I remember writing all kinds of stuff in here."

            yui "Like that time we went camping in the mountains and got lost in the woods. Your mom found us clutching each other crying after calling a search party."

            yui "Or that time I was getting picked on, and you poured milk on the bullys' heads."

            yui "There's all kinds of unforgettable memories I wrote in here. Or at least, that's what I thought."

            yui "I found the part where I went camping, but it wasn't with you, it was with someone named {q}John{/q}."

            yui "When I got bullied, nobody stood up for me. I cried in the bathroom for hours."

            yui "At least, according to this diary. That's not how I remember it at all."

            yui "The last entry in here is from last night. I fell asleep after having a fight with my mom."

            yui "But then, in the middle of the night, I had a dream where a fairy offered to grant me a wish."

            yui "I refused, but before she left she muttered something about finding someone else."

            yui "So, I guess that explains the curse part..."

            pause 1.0

            show yui a_5
            yui "Is our entire friendship a lie?"

            "Sandy's eyes were wide as saucers."

            show sandra c_4
            sandra "N-no!"

            show sandra c_5
            sandra "It's just... things are a bit... different..."

            show sandra c_4
            sandra "But that doesn't make it any less real!"

            pause 1.0

            show sandra c_2
            sandra "You should show this to my mom."

            scene main room night
            show yui a_0 at centerright, faceleft
            show sandra c_5 at centerleft, faceright
            show john a_1 at center
            with dissolve

            show yui a_6
            yui "So, I guess you were originally my friend {q}John{/q}, huh?"

            show john a_4
            "Ms. Davis showed the same expression of utter disbelief her daughter had shown a few minutes prior."

            show yui a_6
            yui "This is all I know you as, but I guess you had a completely different life before that wish, huh?"

            show john a_3
            john "Y-yeah..."

            show john a_6
            john "We've always been close, things just got... reconfigured a bit."

            show john a_5
            john "Though I suppose you and Sandy are a lot closer than... the other configuration."

            pause 1.0

            show yui a_3
            yui "Well, I guess the next step is to figure out what exactly the wish was."

            yui "If it gets granted, then you should be able to wish yourselves back, right?"

            yui "The spell seems to not let you {i}say{/i} anything about it, but there might be another way to communicate..."

            show yui b_5
            extend " like charades!"

            nvl clear

            nvl_narrator "And boy, what a thrilling game of charades it was."

            nvl_narrator "Full of shenanigans, whimsy, and a lot of fun."

            nvl_narrator "I was still feeling a bit down about the whole {q}My best friend is a lie{/q} thing, but at least this let me take my mind it for a bit."

            yui "I see, so {q}John{/q} wished to give his mom a vacation."

            extend " Aw, that's so sweet!"

            show yui b_2
            yui "So you won't get to wish yourselves back until she actually gets a vacation..."

            show yui b_5
            yui "This is perfect!"

            yui "My mom and I are planning a trip to Magic World! If I bring Sandy along, and then once we're back the wish will be granted!"

            show yui b_2
            yui "We won't be able to go until the end of the month, though... can you two hold out until then?"

            "After looking at each other for a moment, they both nodded in unison."

            scene black with dissolve

            "Now that they had a plan for switching back to normal, Sandy and Ms. Davis seemed to relax a lot more."

            "Sandy decided to enjoy her time with me while she still had it, and Ms. Davis went back her normal peppy self as well."

            "...or at least what I thought was normal."

            outfit john formal

            scene bg classroom 3 day
            show john a_9 at centerleft, faceright
            show yui a_0 at centerright, faceleft
            with dissolve

            show john a_10
            john "I hope you all have fun translating these lines from \"Atashi No Sanbyaku Dokidoki Deto\", I thought this would be a nice change of pace!"

            show john a_9
            john "Now, we have a couple minutes left, anyone have any questions about the assignment?"

            show yui b_5
            yui "Yes, I have a quick question! I'm skimming over the script, and I don't quite get this sentence..."

            john "Oh course, Yui, which part?"

            yui "This part here, \"Namagaii\"? Why would she talking about liking raw food when they're at the beach?"

            show john a_8
            john "Oh, they don't literally mean raw as in food, she means doing it ra-"

            "Ms. Davis snatched up my paper."

            show john a_4
            john "H-how did that part get in there?!?"

            "Continuing to skim it, her face reddened more and more."

            "Walking around, she quickly snatched papers off student's desks."

            show john a_7
            john "S-sorry class, there's actually no homework tonight!"

            show john a_2
            john "It looks like I printed out the wrong file... heh heh..."

            show yui b_2
            yui "Oh! Are these sentences too advanced?"

            show john a_1
            john "T-that's exactly right, Yui! I don't want to overwhelm anyone!"

            show yui a_1
            think "Hehehe, Ms. Davis accidentally gave me two copies."

            think "I'm going to work on this all night, she's going to be soo impressed!"

            outfit yui casual

            scene bg yui room night computer on
            show yui a_0 at center
            with dissolve

            show yui a_3
            think "Huh, I don't get this part..."

            text start yui
            text title "Sandy <3"
            msg yui "Heeeey Sandy, could you ask your Mom what \"Nurete Manko\" means?"
            msg sandra "..."
            msg sandra "Whatcha readin?"
            msg yui "Huh? Why's that matter?"
            msg sandra "Just curious :)"
            msg yui "Well, your Mom was going to assign us some lines from an Otome she likes..."
            msg yui "But she snatched it back at the last second!"
            msg yui "I had an extra copy, though!"
            msg sandra "Ah, I see! Don't worry. I'll go ask her."
            msg sandra "Don't wanna risk me getting it wrong."
            msg sandra "brb"
            msg sandra "Ahahahaah she's getting really yelly."
            msg sandra "I told her you snuck her print-out home and now she's getting even yellier"
            msg sandra "I think she's super impressed"
            msg yui "Wow!!!! I won't let her down."
            msg sandra "Oh, also."
            msg sandra "It means Wet Beaver :)"
            msg yui "Awesome, thanks!"
            msg sandra "Oh, and if it comes up"
            msg sandra "\"Ochinchin\" means eggplant."
            msg yui "Thanks, you're a lifesaver!"
            text end

            scene black with dissolve

            "The next day, Ms. Davis begged me to not tell my Mom about it."

            "I didn't really get why a story about eggplants and beavers needed to be kept secret, but I promised anyways."

            outfit john casual

            scene main livingroom day
            show john a_0 at centerleft
            show sandra b_2 at centerright, faceleft
            with dissolve

            "Finally, the day arrived."

            john "Is everything packed?"

            show sandra b_2
            sandra "I think so!"

            show john a_4
            john "Extra changes of clothes? Pajamas? Toothbrush? Sunblock?"

            show sandra d_1
            sandra "Yes, yes, yes, and yes."

            show john a_7
            john "Some books for the drive?"

            show sandra e_0
            sandra "I've got my phone!"

            show john a_6
            john "Oh right, they have books on there now, too."

            show yui a_0 at right, faceleft with moveinright

            show yui a_1
            yui "Hi, Sandy! Ready to go?"

            show sandra b_1
            sandra "Yup! I'm all packed!"

            show yui a_0
            yui "Great! I'll be in the car!"

            show yui a_0 at offscreenright, faceright with moveoutright

            show john a_7
            john "Remember to call me when you get to the hotel!"

            show sandra b_0
            sandra "I will!"

            show john a_9
            john "And make sure you apply sunblock every two hours, I don't want you coming back looked all red!"

            show sandra c_0
            sandra "I know, I know! You told me five times!"

            show sandra c_1
            sandra "Ok, I'm heading out!"

            show john a_10
            john "Not before I get a hug!"

            show sandra c_1:
                xpos 0.4
            with ease
            # go in for hug

            show john a_2
            pause .25

            show sandra c_6:
                xpos 0.35
            with ease

            show john a_0
            john "Bye! Love you!"

            show sandra c_0
            sandra "Love you too!"
            show sandra c_0 at offscreenright, faceright with moveoutright

            scene bg sky day with dissolve

            nvl clear

            nvl_narrator "Amidst all the fun, I'd forget that this wasn't how things were supposed to be."

            nvl_narrator "That we were here to fix this."

            nvl_narrator "That Sandy wasn't supposed to be my best friend."

            show bg sky dusk

            nvl_narrator "After a weekend jam-packed with fun, friends, and fireworks, it was time to head home."

            scene main livingroom dusk
            show john a_6 at center
            with dissolve

            john "They said they'd be back a half hour ago... where are they?"

            show sandra b_0 at centerright, faceleft with moveinright
            show yui a_0 at right, faceleft with moveinright

            sandra "Hi, mom!"

            yui "Hi, Ms. Davis!"

            show john a_2
            john "Girls! You're back!"

            show sandra c_1
            sandra "Yeah, we are. Back from a nice, fun vacation!"

            show john a_9
            john "Where's Yuuna?"

            show yui a_1
            yui "I told her you'd drop me off at home in a couple hours."

            pause 1.0

            show yui a_5
            yui "Well... I guess this is it."

            show sandra d_5
            sandra "I guess it is..."

            show circe a_5 at left with dissolve
            show john a_5 at faceleft
            circe "Look at that, wish granted! Wasn't that a ton of fun?"

            circe "Way better than just snapping my fingers and just making it so, right?"

            show circe a_0
            circe "Oh hey, didn't expect to see you again, girl."

            show circe a_5
            circe "Want to make your own wish? I'm always ready and willing."

            show yui a_6
            yui "Uh, that's ok, but we would like to switch these two back, please..."

            pause 1.0

            show circe a_2
            circe "And how did you know they were switched in the first place?"

            yui "..."

            # nervous

            show yui b_2
            yui "M-magic?"

            circe "..."

            show circe a_0
            circe "Mage, huh? Always show up in the damndest places..."

            circe "Well, I guess I could grant that wish, but that doesn't seem like any fun at all."

            show circe a_3
            circe "If you wish for them to be switched back, then you'll just get what you want right away! There's no drama! No excitement!"

            pause 1.0

            show circe a_0
            circe "Ok, here's what we'll do."

            circe "I'll give you five minute to pick one person."

            circe "Whoever you choose, I'll let wish for anything {i}other than{/i} switching these two back."

            circe "Buuuuut, if fulfilling that wish requires me to switch them back, then I will!"

            show circe a_5
            circe "So, who wants to be our lucky contestant?"

            "An hour glass appeared next to her."

            circe "I'll give you five minutes to decide."

            show circe a_0
            circe "Oh, and this will be the last wish I grant for any of you."

            circe "You've gotten a little too wise for my liking."

            circe "Aaaaaaaaand, start!"

            hide circe with dissolve

            circe "See you in five!"

            nvl clear

            nvl_narrator "What followed were several tense minutes of discussion, weighing each of the options."

            nvl_narrator "Ms. Davis had made the original wish, so she had the most experience with this system."

            nvl_narrator "On the other hand, Sandy had a couple good ideas, herself."

            nvl_narrator "Lastly, I might have a good shot as well, since I had rejected the original wish opportunity, and was able to figure out what was going on."

            nvl_narrator "That said, Sandy and Ms.Davis were worried that making a wish myself might wrap me up in what was their mess."

            show john a_5 at faceright
            john "We'll need to be careful this time, they're going to take any opportunity they can get to twist our words around."

            menu:
                "When the sand ran out, we finally decided on..."
                "Ms. Davis":
                    show yui a_3
                    yui "Ms. Davis made the last wish, so she should make this one, too."

                    yui "She has the most experience, after all!"

                    show john a_9
                    john "That makes sense."

                    show sandra a_6
                    sandra "Works for me."

                    show circe a_3 at left with dissolve
                    circe "I'm back!"

                    circe "Make your decision?"

                    show john a_3 at faceleft
                    john "Yes, I'll do it."

                    show circe a_0
                    circe "You again, huh?"

                    "Several glowing rings surrounded Ms. Davis, and she began floating in the air."

                    show circe a_5
                    circe "Alright, off we go!"

                    show sandra a_4
                    sandra "W-wait! What're taking her?"

                    show circe a_0
                    circe "Oh, just somewhere private."

                    circe "Don't worry, I won't hurt your dear, dear, mother."

                    extend " Or son. Or whatever they end up wishing themselves into."

                    show circe a_5
                    circe "Tata!"

                    "With a flash of white smoke, Ms. Davis and the fairy vanished."

                    "And we started to... lose consciousness..."

                    show black with dissolve

                    body john johnGB
                    outfit john casual

                    $ john.name = "Jane"

                    body holly strangesunday_hollyAP
                    outfit holly casual

                    $ holly.name = "Holly Davis"

                    sandra "...ake?"

                    sandra "...ok, Yui?"

                    scene main livingroom dusk
                    show yui b_2 at center, faceleft
                    show sandra a_3 at centerleft, faceright

                    yui "Wah!"

                    yui "Did the wish work?!? Where's Ms. Davis-{nw}"

                    yui "Did the wish work?!? Where's{fast} Jane?"

                    show john a_1 at centerright, faceleft with dissolve

                    john "R-right here..."

                    show yui b_5 at faceright
                    yui "Wow! It worked! You're my age again!"

                    show yui b_2
                    yui "Though you are still a girl..."

                    yui "And you didn't change at all, Sandy!"

                    show john a_9
                    john "Guess I wasn't specific enough."

                    show yui b_2
                    yui "Yeah, guess so."

                    yui "Wait, hold on..."

                    show yui at faceleft
                    yui "If you're a teenager..."

                    show yui at faceright
                    extend " and you're a teenager..."

                    extend " then who's..."

                    show yui at faceleft
                    show sandra at faceright
                    show holly a_5 at left with moveinleft
                    holly "-going to explain what's going on, here?"

                    show yui a_6
                    yui "Oh!"

                    show holly a_3
                    holly "How am I suddenly in my late-thirties?"

                    show holly a_2
                    holly "And Jane's a kid?"

                    outfit strangesunday_taylor nurse
                    $ strangesunday_taylor.name = "Taylor Davis"

                    show yui at faceright
                    show sandra at faceleft
                    show john at faceright
                    show strangesunday_taylor a_1 at right, faceleft with moveinright

                    strangesunday_taylor "Honey, I'm hooooooooome!"

                    show strangesunday_taylor a_0
                    strangesunday_taylor "Hey kiddos, enjoy your trip?"

                    show holly a_3
                    holly "Who are-"

                    show strangesunday_taylor a_8
                    strangesunday_taylor "Sorry, I'm sweating buckets after that shift. Need to go clean off."

                    show strangesunday_taylor a_1:
                        xpos .25
                    with ease
                    strangesunday_taylor "Though if you wanna join me in the shower, I won't mind."

                    show strangesunday_taylor a_1 at offscreenleft with moveoutleft

                    "After giving Holly a peck on the cheek the mysterious woman{nw}"

                    "After giving Mrs. Holly Davis a peck on the cheek, Mrs. Taylor Davis{fast} trotted up the stairs and into the bathroom."

                    show yui b_2 at faceleft
                    yui "She was wearing a ring, and it matches the ring that you're wearing."

                    show yui b_5
                    yui "I think that's your wife!"

                    pause 1.0

                    show holly a_0
                    holly "Nice."

                    pause 1.0

                    show holly a_5
                    holly "Well uh,"

                    extend " I'm just gonna"

                    extend " go ahead and"

                    show holly a_0
                    extend " join her, then."

                    show holly a_0 at offscreenleft with easeoutleft
                    "Mrs. Davis rushed up the stairs."

                    sandra "Huh."

                    show sandra f_0
                    sandra "Looks like Jane's wish resulted in {i}two{/i} moms."

                    show sandra g_1
                    sandra "Guess that's why it was limited to two wishes, right? Any more and we've be over the legal limit!"

                    show yui a_3
                    yui "Ugh."

                    show john a_8
                    john "Ugh."

                    # NVL for aftermath
                    nvl clear

                    nvl_narrator "When the fairy prompted Jane for a wish, she wished {q}my daughter was the responsible one{/q}."

                    nvl_narrator "Jane's age was reverted, but I guess she didn't specify which daughter she wanted made more responsible."

                    nvl_narrator "There was nothing in there about gender either, so a girl she stayed."

                    nvl clear

                    nvl_narrator "In this reality, Holly and Taylor met in college and married shortly after."

                    nvl_narrator "When they were ready to have kids, they decided to both get pregnant at the same time."

                    nvl_narrator "Thanks to recent advances in medicine, the kids were still a mixture of both their DNA, too."

                    nvl_narrator "This resulted in Sandra and Jane, sisters of the same age."

                    outfit strangesunday_taylor casual

                    scene bg main kitchen dusk
                    show yui a_0 at left
                    show john a_3 at centerleft, faceright
                    show sandra a_0 at center, faceright
                    show holly a_0 at centerright, faceleft
                    show strangesunday_taylor a_0 at right, faceleft
                    with dissolve

                    holly "How'd you girls like your college orientation?"

                    show yui a_1
                    yui "It was great! Their campus is huuuuuuge!"

                    show sandra a_1
                    sandra "Yeah, plus Jane was fawning over those instructors."

                    show john a_2
                    john "I-I was not!"

                    show sandra a_2
                    sandra "You were practically drooling!"

                    show holly a_1
                    holly "Oh really? Jane's got a crush already? How cute!"

                    show sandra a_0
                    sandra "Crushes."

                    show holly a_5
                    holly "Male? Female?"

                    show sandra a_1
                    sandra "Both."

                    show holly a_1
                    holly "Nice."

                    show john a_13
                    john "Nooooooooo!!!"

                    holly "What? I'm being supportive!"

                    show strangesunday_taylor a_8
                    strangesunday_taylor "Now now, let's not keep antagonizing her."

                    show strangesunday_taylor a_1
                    strangesunday_taylor "So, did you pick out your schedules? What's your first semester look like?"

                    nvl clear

                    nvl_narrator "Even though I wasn't directly affected by the wish, I still retained my memories of the wish taking place, and how things were when Jane was the Mom."

                    nvl_narrator "I still didn't have any memories of Jane as a boy, though."

                    nvl_narrator "I was able to freely talk about it with Sandy, Jane, and Holly, but if I talked to anyone else, my speech would be corrected."

                    scene bg yui livingroom day
                    show yui b_0 at centerright, faceleft
                    show yuuna a_0 at centerleft, faceright
                    with dissolve

                    yui "Jane was so flustered she had to hide in the bathroom for an hour!"

                    show yui b_2
                    yuuna "Hmm? Why would Jane accidentally sit cross-legged in a skirt? Was she having a bad day?"

                    show yui b_5
                    yui "Oh, it's because Jane has always been a girl!"

                    yui "She's completely used to wearing skirts, because she has been her whole life."

                    show yuuna a_0
                    yuuna "Riiiiiiiight."

                    show yui b_2
                    yui "Uh, what I meant was..."

                    show yui b_0
                    extend " she stayed up all night playing \"Niwatori Boyfriend\"."

                    show yuuna a_1
                    yuuna "Aw, Jane and her stories!"

                    "It wasn't quite back to normal for everyone."

                    "But at least we were happy."

                    scene black with fade

                    gameover "Happy Holly Days"

                    # YOU THOUGHT URCICELLO WAS THE ONLY ONE THAT COULD DO PUNS WELL YOU WERE WRONG

                "Sandy":
                    show yui a_3
                    yui "Sandy had a couple of good ideas, I think she should do it!"

                    show sandra a_1
                    sandra "I won't let you down!"

                    show circe a_5 at left with dissolve
                    circe "I'm back!"

                    show circe a_0
                    show john at faceleft
                    circe "Make your decision?"

                    show sandra a_6
                    sandra "Yeah, I'll make the wish."

                    show circe a_3
                    circe "The mother turned daughter, huh? Interesting!"

                    "Several glowing rings surrounded Sandy, and she began floating in the air."

                    circe "Alright, off we go!"

                    show john a_4
                    john "W-wait! What're you doing?"

                    show circe a_5
                    circe "Oh, we're just going somewhere private."

                    show circe a_3
                    circe "Don't worry, I won't hurt your dear daughter!"

                    show circe a_0
                    extend " Or mother. Or whatever they end up wishing themselves into."

                    circe "Tata!"

                    show black with dissolve

                    "With a flash of white smoke, Sandy and the fairy vanished."

                    "And we started to... lose consciousness..."

                    body john johnGB
                    outfit john casual

                    $ john.name = "Jane"

                    body sandra sandra
                    outfit sandra casual

                    $ sandra.name = "Sandra Davis"

                    sandra "...ake?"

                    sandra "...ok, Yui?"

                    scene main livingroom dusk
                    show yui b_2 at center
                    show sandra a_2 at centerright

                    yui "Euugh!"

                    show yui b_5
                    yui "Sandy! You're an adult again!"

                    show sandra a_1
                    sandra "Looks like it, Yui."

                    show yui b_2
                    yui "Where's Ms. Davis{nw}"

                    yui "Where's{fast} Jane?"

                    show john a_1 at centerleft with dissolve
                    john "Over h-here..."

                    yui "Wow! It worked! You're my age again!"

                    show yui b_2 at faceleft
                    yui "Though you are still a girl..."

                    show sandra a_5
                    sandra "Hmm, I didn't specify our genders with my wish, so it looks like that demon took advantage of that."

                    show john a_2
                    john "But why are you still a girl then?"

                    show sandra a_9
                    sandra "{s}Finding adult male sprints is pain in the ass{/s} Maybe they were feeling merciful?"

                    nvl clear

                    nvl_narrator "When the fairy prompted Sandy, she said {q}I wish I could give {i}my{/i} mom a vacation!{/q}."

                    nvl_narrator "Since Ms. Davis made the same wish Jane did, their ages were swapped back."

                    nvl_narrator "She didn't mention anything about gender, though, so Jane stayed a girl."

                    scene bg main kitchen dusk
                    show yui a_0 at left
                    show john a_3 at centerleft, faceright
                    show sandra a_9 at center, faceleft
                    show holly a_0 at centerright, faceleft
                    with dissolve

                    sandra "How'd you kids enjoy college orientation?"

                    show john a_2
                    john "It was good!"

                    show yui a_1
                    yui "Their library is amazing! There's so many books!"

                    show sandra a_1
                    sandra "Did your sister show you all the best places to hook up, Jane?"

                    show holly a_1
                    holly "You know it!"

                    show john a_17
                    john "We did not!"

                    show yui b_2
                    yui "Was that what you two were doing when I was looking for the science building?"

                    show holly b_1
                    holly "It totally was. She even got a girl's number."

                    yui "That fast?!"

                    show john a_13
                    john "They're just a tutor! And it was a buisness card! That doesn't count at all!"

                    show john a_11
                    john "{size=-5}{i}Even if they were cute...{/i}{/size}"

                    # Since Yui was involved in the wish, she was immune to reality changes this time and able to freely talk about it

                    # If she ever tried to talk to anyone else, though, it didn't work.
                    # Yui is telling Yuuna a story that's funny because John is now Jane

                    scene bg main livingroom dusk
                    show yui a_5 at centerright, faceleft
                    show sandra a_1 at centerleft, faceright
                    with dissolve

                    sandra "Bye Yui, say hi to your mom for me!"

                    yui "..."

                    show sandra a_2
                    sandra "Something wrong?"

                    show yui a_6
                    yui "I..."

                    show yui a_5
                    extend "  miss you..."

                    pause 1.0

                    show sandra a_1
                    sandra "But I'm right here, Yui! You ok?"

                    show yui a_6
                    yui "You know what I mean."

                    pause 1.0

                    show sandra a_6
                    sandra "Yeah, I do."

                    show sandra a_7
                    sandra "I miss it too."

                    pause 1.0

                    show sandra a_2
                    sandra "But hey, you and Jane seem to be a lot closer!"

                    sandra "And Holly's come back out of her shell, too."

                    show yui a_5
                    yui "Yeah, and that's nice and all, but..."

                    pause 1.0

                    show sandra a_2
                    sandra "Yeah, I know."

                    show sandra a_5
                    sandra "We're back in different generations."

                    show sandra a_2
                    sandra "You'll be at the drinking age soon, right? Let's go to a bar sometime."

                    show sandra a_1
                    sandra "First round's on me."

                    show yui a_1
                    yui "I'd like that."

                    show yui a_0 at offscreenright with moveoutright

                    scene black with fade

                    gameover "Life's a Beach Without Sandy"

                "Me":
                    show yui a_3
                    yui "I'll do it."

                    "I held up my diary."

                    yui "I have an idea on how to beat her."

                    show john a_0
                    john "Ok. I trust you."

                    show sandra a_2
                    sandra "Yeah, me too."

                    show circe a_3 at left
                    circe "I'm back!"

                    show circe a_5
                    show john at faceleft
                    circe "Make your decision?"

                    yui "Yes. I'll do it."

                    show circe a_0
                    circe "The prying third party, eh? Didn't expect that!"

                    "Several glowing rings surrounded me, and I began floating in the air."

                    show circe a_3
                    circe "Alright, off we go!"

                    sandra "W-wait! Where are you taking here!?"

                    show circe a_5
                    circe "Oh, we're just going somewhere private."

                    circe "Don't worry, I won't hurt your friend."

                    circe "Tata!"

                    "With a flash of white smoke, the room completely transformed around me."

                    scene bg yui room dusk study
                    with dissolve

                    show yui a_3 at center
                    show circe a_0 at centerright, faceleft
                    with dissolve

                    yui "Where... are we?"

                    circe "Somewhere private. I figured your room would work."

                    show circe a_3
                    circe "As much as I'd love to take you to the demon realm, your skin would melt off!"

                    circe "That wouldn't be any fun at all!"

                    show yui a_2
                    yui "{b}You're a demon!?!{/b}"

                    yui "{b}But you said you were a fairy!!!{/b}"

                    show circe a_1
                    circe "..."

                    show circe a_2
                    circe "Wow. For someone as genre-saavy as you I thought you'd have caught on sooner."

                    show circe a_5
                    circe "Ah well, that's neither here nor there."

                    circe "What's you're wish?"

                    show yui a_3
                    yui "One moment, please!"

                    "I pulled out my diary, and began furiously writing a new entry."

                    think "Just saying a sentence can be misinterpreted, but if I write out a whole page of text, it'll be impossible to mess up!"

                    show circe a_0
                    circe "Hmm? Trying the ol contract approach? That's fun."

                    show circe a_3
                    circe "Make sure you don't make any typos, I follow these to the letter!"

                    pause 3.0

                    show circe a_0
                    circe "Hey, you done yet? Don't have all day here."

                    circe "Ok that's a lie, watching you write is just boring."

                    show yui b_1
                    yui "Almost."

                    "..."

                    show circe a_2
                    circe "Pheeeeew..."

                    "..."

                    show circe a_0
                    circe "Booooooooored."

                    "..."

                    show yui a_3
                    yui "Done!"

                    show yui a_2
                    yui "I just wrote a diary entry for tomorrow! I wish that all the events in it come true to the letter."

                    show circe a_5
                    circe "Great! Let's see here, then."

                    "My diary poofed out of my hands and into hers."

                    "Reading it over, she grinned."

                    show circe a_3
                    circe "Pretty detailed, but not detailed enough!"

                    circe "It cuts off midway through the day!"

                    show yui b_1
                    yui "There's another page."

                    "The demon turned the page."

                    show yui b_5
                    yui "And there's ten more after that!"

                    show circe a_2
                    "Her grin was slowly wiped from her face"

                    show circe a_1
                    extend " and then replaced with a look of disgust."

                    "She began leafing through the other pages of the diary, too."

                    circe "Well shit! That's how you were so wise on this. The book must have become immune to my magic when I licked it!"

                    "She let out a long sigh."

                    show circe a_2
                    circe "Well, either way, you got me. Can't find anything to exploit at all."

                    show circe a_0
                    circe "You should be a lawyer or something."

                    "The fairy snapped her fingers, and another white cloud of smoke blasted out of her."

                    circe "Enjoy going back to your boring life, human."

                    show black with dissolve

                    body john john
                    outfit john casual

                    $ john.name = "John"

                    body sandra sandra
                    outfit sandra casual

                    $ sandra.name = "Sandra Davis"

                    sandra "...ake?"

                    sandra "...ok, Yui?"

                    scene main livingroom dusk
                    show yui a_0 at center
                    show sandra a_0 at centerright

                    yui "Euugh!"

                    show yui b_5
                    yui "Sandy! You're an adult again!"

                    show sandra a_1
                    sandra "Yeah, looks like it."

                    show yui b_2
                    yui "Where's John?"

                    show john a_2 at centerleft with dissolve
                    john "Over here..."

                    john "Ugh, my head..."

                    show yui b_0 at faceleft
                    yui "Wow! It worked! You're my age again! And a boy!"

                    show john a_1
                    john "Woah I am! You did it!"

                    show john a_0
                    john "Man, I really owe you one, Yui!"

                    show sandra a_9
                    sandra "Yeah, we both owe you a lot."

                    show sandra a_8
                    sandra "How did you manage to get that demon set everything back to normal, though?"

                    show yui b_5
                    yui "The premise of my wish was simple. If I wrote out a completely normal day in my diary, and asked to make it so, Circe would be bound to that."

                    yui "So tomorrow, I'm going to wake up at the normal time, go to school, and chide John for being late for class."

                    yui "Ms. Davis will Natsumi in the evening while my Mom has a showing, and my mom and I will pick her up when she's done."

                    show yui b_0
                    yui "It's a bit more detailed than that, but you get the idea."

                    show black with dissolve

                    nvl clear

                    nvl_narrator "And so it happened. Everything that happened the next day was completely ordinary."

                    nvl_narrator "No swapped lives, no gender changes, just another normal day in the life of Yui Yamashita."

                    nvl_narrator "Sometimes I'd reminise on how fun it was to hang out with Sandy, or good of a teacher Jane Davis was."

                    nvl_narrator "But as time went on, the whole thing felt like more and more of a dream."

                    nvl_narrator "Well, except for one thing..."

                    # yuuna, sandra, and john are running late for a concert

                    yuuna "C... D... E... ah, here are are! F6-10!"

                    sandra "Who wants the aisle seat?"

                    john "I'll take it."

                    natsumi "Does everybody have their glowsticks!?"

                    john "Yes, you can stop asking."

                    yuuna "Oh, it's starting!"

                    # yuuna and sandra are exited, john is pretending her's only here to be supportive but he enjoys it

                    # They get in, Yui is an idol and performing

                    scene strangesunday_bg idol with dissolve

                    yui "Hi everyone, are you ready for my magic spell?"

                    yui "Cause I'm about to cast it over the whole world!"

                    "I may have added a small extra part about becoming an idol."

                    show black with dissolve

                    gameover "My Ordinary Life"

                "Not making a wish":
                    john "Maybe..."

                    show john a_3
                    extend " we should give it up."

                    show yui a_2
                    yui "What? Why?"

                    show john a_7
                    john "You heard her! She's going to twist whatever we say against us!"

                    show john a_3
                    john "And I mean, the way we are now... isn't really that bad."

                    john "I kind of..."

                    show john a_2
                    extend " like it."

                    pause 1.0

                    show sandra a_5
                    sandra "Me too."

                    show yui b_2
                    yui "Sandy, you too!?"

                    show circe a_0 at left with dissolve

                    circe "I'm baaaaaack!"

                    show circe a_0
                    circe "Made a decision?"

                    show john at faceleft
                    john "Yes."

                    show john a_7
                    john "We're going to stay like this!"

                    circe "..."

                    show circe a_1
                    circe "Well, I did want to play a little more, but if you insist, I guess I can just leave!"

                    "The fairy threw a card to Ms. Davis."

                    show circe a_5
                    circe "If you ever change your mind, just give me a call."

                    hide circe with dissolve
                    "With one last blast of white smoke, the fairy disappeared."

                    nvl clear

                    nvl_narrator "With the fairy gone, it looked like this life really was going to be permanent."

                    nvl_narrator "To be honest, I felt relieved."

                    nvl_narrator "It was selfish, but I didn't want them to switch back."

                    scene bg main kitchen dusk
                    show yui a_0 at left, faceright
                    show sandra a_0 at centerleft, faceright
                    show john a_2 at center, faceleft
                    show holly b_0 at centerright, faceleft
                    with dissolve

                    john "Have fun at college orientation?"

                    show yui b_5
                    yui "It was great!"

                    show john a_9
                    john "Sandy wasn't too much trouble, was she?"

                    show holly b_1
                    holly "Nah, she was surprisingly well behaved."

                    show sandra g_1
                    sandra "Yeah, gotta look good for all those cute guys walking around..."

                    yui "Oh, so now they're not too young for you?"

                    show sandra f_2
                    sandra "Hmm? Why would I think that?"

                    show yui a_6
                    yui "{size=-5}{i}You used to be an adult?{/i}{/size}"

                    pause 1.0

                    show sandra e_0
                    sandra "Oh, right! Ha ha!"

                    sandra "Yeah, I think I'm well over that."

                    show sandra f_1
                    sandra "Though speaking of older men, how was your date with Mr. Carter, Mom?"

                    show john a_7
                    john "For the last time, it wasn't a date!"

                    show john a_6
                    john "We were just getting dinner. As friends."

                    show sandra c_0
                    sandra "Uh huh."

                    show sandra c_6
                    sandra "Friends that were making out on the doorstep."

                    show john a_7
                    john "I thought you were at Yui's!"

                    sandra "I was. I didn't {i}see{/i} anything."

                    show sandra g_1
                    extend " But thanks for confirming my suspicions."

                    john "!!!"

                    show sandra e_0
                    sandra "So when are you getting married?"

                    pause 1.0

                    show john a_6
                    john "N-none of your business!"

                    scene black with fade

                    "As they kept adjusting to their new lives, they forgot more and more from their old lives."

                    "They even started forgetting that they had swapped in the first place."

                    "But we were still happy, and that's all that mattered to us."

                    gameover "Jane Still a Sensei"